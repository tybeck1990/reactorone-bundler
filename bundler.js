'use strict';

const _ = require('lodash');
const babelify = require('babelify');
const uglifyify = require('uglifyify');
const browserify = require('browserify');
const Promise = require('bluebird');
const globby = require('globby');
const mkdirp = require('mkdirp');
const sass = require('node-sass');
const minimist = require('minimist');

const {access} = require('fs');
const {join, resolve, basename} = require('path');

const writeFile = Promise.promisify(require('fs').writeFile);

class Bundler {

    constructor () {
        this.staticCommonName = 'tn-common-static';

        this.args = minimist(process.argv.slice(2));
        this.cwd = process.cwd();

        this.rootPath = join(this.cwd, '..');
        this.staticCommonPath = join(this.rootPath, this.staticCommonName);
        this.distPath = join(this.cwd, 'dist');
        this.commonSourceDirectory = join(this.staticCommonPath, 'src');
        this.extraPathDefault = `/reactorone/static/${basename(this.cwd)}`;
    }

    /**
     * @method _hasCommonAssets
     * @returns {Promise<any>}
     * @private
     */
    _hasCommonAssets () {
        return new Promise((resolve, reject) => {
            access(this.staticCommonPath, err => {
                if (err) {
                    return reject(err);
                }
                resolve(true);
            });
        });
    }

    /**
     * @method _getPackage
     * @returns {*}
     * @private
     */

    _getPackage () {
        const uriPkg = `${this.cwd}/package.json`;
        const pkg = require(uriPkg);

        return pkg;
    }

    /**
     * @method _getCssEntryPoint
     * Get css entry point from package file
     * @returns {*|boolean}
     * @private
     */
    _getCssEntryPoint () {
        const pkg = this._getPackage();
        if (pkg && pkg.style) {
            return pkg.style;
        }
        return false;
    }

    /**
     * @method _getJsEntryPoint
     * Get js entry point from package file
     * @returns {*|boolean}
     * @private
     */
    _getJsEntryPoint () {
        const pkg = this._getPackage();
        if (pkg && pkg.main) {
            return pkg.main;
        }
        return false;
    }

    /**
     * @method _bundleJs
     * Bundle all js
     * @returns {Promise<any>}
     * @private
     */
    _bundleJs () {
        return new Promise((resolve, reject) => {
            this
                .bundler
                .bundle((err, data) => {
                    if (err) {
                        reject(err);
                    }
                    const writes = [
                        writeFile(`${this.distPath}/bundle.js`, data),
                        this._extraWrite(this.extraPathDefault || this.args.extraPath, 'bundle.js', data)
                    ];
                    return Promise
                        .all(writes)
                        .then(() => resolve(true));
                });
        });
    }

    /**
     * @method _extraWrite
     * @param directory
     * @param filename
     * @param data
     * @private
     */
    _extraWrite(directory, filename, data) {
        return new Promise((resolve, reject) => {
            mkdirp(directory, err => {
                if (err) {
                    return reject(err);
                }
                return writeFile(join(directory, filename), data)
                    .then(() => {
                       resolve(true);
                    });
            });
        });
    }

    /**
     * @method _resolveUrlDependency
     * @param url
     * @returns {*}
     * @private
     */
    _resolveUrlDependency (url) {
        const _clientNodeModules = resolve('node_modules', url),
            _commonNodeModules = resolve(this.staticCommonPath, 'node_modules', url);
        let urlMappings = [
            `${_commonNodeModules}.css`,
            `${_commonNodeModules}.scss`,
            `${_clientNodeModules}.css`,
            `${_clientNodeModules}.scss`
        ];
        if (~url.indexOf('.')) {
            urlMappings = [
                `${_commonNodeModules}`,
                `${_clientNodeModules}`
            ];
        }
        return Promise.mapSeries(urlMappings, urlMapping => {
            return new Promise(resolve => {
                access(urlMapping, err => {
                    resolve(err ? null : urlMapping);
                });
            });
        })
            .then(resolvedPaths => {
               const paths = resolvedPaths.filter(resolvedPath => {
                  if (resolvedPath) {
                      return resolvedPath;
                  }
               });
               return _.get(paths, '[0]') || null;
            });
    }

    /**
     * @method _bundleCss
     * Bundle resources from `tn-common-static` and `<client>` together
     * @private
     */
    _bundleCss () {
        const self = this;
        return new Promise((resolve, reject) => {
            let outputStyle = 'nested';
            if (this.args.prod) {
                outputStyle = 'compressed';
            }
            sass.render({
                outputStyle,
                file: this._getCssEntryPoint(),
                includePaths: [
                    `${this.cwd}/node_modules/`,
                    `${this.commonSourceDirectory}`,
                    `${this.staticCommonPath}/node_modules/`
                ],
                importer: function importer (url, prev, done) {
                    if (url[0] === '~') {
                        const _url = url.substr(1);
                        self
                            ._resolveUrlDependency(_url)
                            .then(resolvedPath => {
                                done({ file: resolvedPath });
                            });
                        return;
                    }
                    return { file: url };
                }
            }, (err, result) => {
                if (err) {
                    return reject(err);
                }
                const writes = [
                    writeFile(`${this.distPath}/bundle.css`, result.css),
                    this._extraWrite(this.extraPathDefault || this.args.extraPath, 'bundle.css', result.css)
                ];
                return Promise
                    .all(writes)
                    .then(() => resolve(true));
            });
        });
    }

    /**
     * @method _initJavaScriptBundler
     * @returns {Bundler}
     * @private
     */
    _initJavaScriptBundler () {
        this.bundler = browserify({
            entries: [this._getJsEntryPoint()],
            cache: {},
            packageCache: {},
            paths: ['./node_modules/']
        });

        return globby([`${this.commonSourceDirectory}/**/*.js`])
            .then(jsFiles => {
                return Promise.mapSeries(jsFiles, jsFile => {
                    this.bundler.add(jsFile);
                })
                    .then(() => {
                        this.bundler.transform(babelify);
                        if (this.args.prod) {
                            this.bundler.transform(uglifyify, { global: true });
                        }
                        return true;
                    });
            });
    }

    /**
     * @method _makeDistFolder
     * Create the dist directory where our bundled assets will go
     * @private
     */
    _makeDistFolder () {
        return new Promise((resolve, reject) => {
            mkdirp(this.distPath, err => {
               if (err) {
                   return reject(err);
               }
               resolve(true);
            });
        })
    }

    /**
     * @method run
     * @returns {Promise<any | never>}
     */
    run () {
        let env = 'development';

        this.startTime = new Date().getTime();

        if (this.args.prod) {
            env = 'production';
        }

        console.log(`Running in ${env} mode.`);

        return this
            ._hasCommonAssets()
            .then(() => {
                return Promise.all([
                    this._initJavaScriptBundler()
                ])
                    .then(values => {
                        if (!~values.indexOf(false)) {
                            return this
                                ._makeDistFolder()
                                .then(() => {

                                    return Promise.all([
                                        this._bundleJs(),
                                        this._bundleCss()
                                    ])
                                        .then(() => {

                                            this.endTime = new Date().getTime();

                                            return Promise.resolve({
                                                status: true,
                                                runtime: (this.endTime - this.startTime) / 1000
                                            });

                                        });

                                });
                        }
                        return Promise.reject('Ran into issues while trying to initialize css/js bundlers.');
                    });
            });
    }
}

module.exports = Bundler;