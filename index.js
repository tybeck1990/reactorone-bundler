'use strict';

const Bundler = require('./bundler');

(bundler => {
    console.log('Started bundling process...');

    return bundler
        .run()
        .then(stats => {
            console.log(`Runtime: ${stats.runtime}s`);
            console.log(`Bundler finished!`);
        })
        .catch(e => console.log(e));
})(new Bundler());